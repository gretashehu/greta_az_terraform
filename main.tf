resource "azurerm_virtual_machine" "greta-vm" {
    name = "greta-vm"
    location = azurerm_resource_group.gretarg.location
    resource_group_name = azurerm_resource_group.gretarg.name
    network_interface_ids = [azurerm_network_interface.greta-nic.id]
    vm_size = "Standard_DS1_v2"
    delete_os_disk_on_termination = true 
    delete_data_disks_on_termination = true


    storage_image_reference {
        id = "/subscriptions/c0177cf4-3641-4a06-bbf6-40d1ac0a60f8/resourceGroups/gretarg/providers/Microsoft.Compute/images/greta_packer_image"
        publisher = ""
        offer = ""
        sku = ""
        version = ""
    }

    storage_os_disk {
        name = "gretaosdisk11"
        caching = "ReadWrite"
        create_option = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    os_profile {
        computer_name = "gretavm"
        admin_username = "greta"
        admin_password = "greta@1234"
    }

    os_profile_linux_config {
        disable_password_authentication = false
    }
}

resource "azurerm_resource_group" "gretarg" {
    name = var.name
    location = var.location
}

resource "azurerm_storage_account" "gretasa" {
  name                     = "gretashehusa"
  resource_group_name      = azurerm_resource_group.gretarg.name
  location                 = azurerm_resource_group.gretarg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
resource "azurerm_storage_container" "gretasac" {
  name                  = "containeruno"
  storage_account_name  = azurerm_storage_account.gretasa.name
  container_access_type = "private"
}

resource "azurerm_virtual_network" "gretavn" {
  name                = "gretavn"
  location            = azurerm_resource_group.gretarg.location
  resource_group_name = azurerm_resource_group.gretarg.name
  address_space       = ["10.0.0.0/16"]

}

resource "azurerm_subnet" "gretasubnet" {
    name = "gretavnsubnet"
    resource_group_name = azurerm_resource_group.gretarg.name
    virtual_network_name = azurerm_virtual_network.gretavn.name
    address_prefixes = ["10.0.3.0/24"]
}

resource "azurerm_network_security_group" "gretasg" {
  name                = "greta-seciurity-group"
  location            = azurerm_resource_group.gretarg.location
  resource_group_name = azurerm_resource_group.gretarg.name
  
  
  security_rule {
    name                       = "allow-on-port-433"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "433"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow-on-port-80"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow-ssh-office-ip"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "80.91.117.70"
    destination_address_prefix = "*"
  }

  
}

resource "azurerm_subnet_network_security_group_association" "nsg-association" {
  subnet_id = azurerm_subnet.gretasubnet.id
  network_security_group_id = azurerm_network_security_group.gretasg.id
  depends_on = [
    azurerm_network_security_group.gretasg
  ]
}


resource "azurerm_public_ip" "greta-public-ip" {
  name                = "greta-public-ip"
  resource_group_name = azurerm_resource_group.gretarg.name
  location            = azurerm_resource_group.gretarg.location
  allocation_method   = "Dynamic"

}

resource "azurerm_network_interface" "greta-nic" {
    name = "greta-nic"
    location = azurerm_resource_group.gretarg.location
    resource_group_name = azurerm_resource_group.gretarg.name


    ip_configuration {
        name = "greta-config"
        subnet_id = azurerm_subnet.gretasubnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id = azurerm_public_ip.greta-public-ip.id
    }
}

data "azurerm_image" "gretapacker" {
   name                = var.gretapacker
   resource_group_name = var.gretarg
}


