terraform {
  required_providers {
    azurerm = {
        source = "hashicorp/azurerm"
        version = "=3.48.0"

    }
  }

backend "azurerm" {
  resource_group_name  = "gretarg"
  storage_account_name = "gretashehusa"
  container_name       = "containeruno"
  key                  = "terraform.tfstate"
  }

}

provider "azurerm" {
    features{}
}

